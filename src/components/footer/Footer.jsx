import Image from "next/image"
import styles from './page.module.css'

const Footer = () => {
  return (
    <footer className={styles.container}>
      <div>2023 Next Pet App. All rights reserved</div>
      <div className={styles.social}>
        <Image src='/twitter.png' width={15} height={15} className={styles.icon} alt="x.com"/>
        <Image src='/inst.png' width={15} height={15} className={styles.icon} alt="instagram.com"/>
        <Image src='/yt.png' width={15} height={15} className={styles.icon} alt="youtube.com"/>
      </div>
    </footer>
  )
}

export default Footer